load('api_mqtt.js');
load('api_timer.js');
load('api_sys.js');
load('api_config.js');
load('api_rpc.js');
load('api_gpio.js');
load('api_arduino_bme280.js');


let stateSwitchEsp = {
    stateInput1: null,
    stateInput2: null,
    staseInput3: null,
    stateInput4: null,
    pins: [12, 14, 15, 13],
    timeZone: 7,  // временная зона +7 
};

for (let i = 0; i < stateSwitchEsp.pins.length; i++) {
    GPIO.setup_output(stateSwitchEsp.pins[i], 1);
};

let bme = Adafruit_BME280.createI2C(0x76);
let bmeValue = null;

RPC.addHandler('myRpc', function (jsonText) {
    let textSaved = JSON.stringify(jsonText);
    if (jsonText.id === 'input1') {
        Cfg.set({ input1: textSaved });
    } else if (jsonText.id === 'input2') {
        Cfg.set({ input2: textSaved });
    };
    print(JSON.stringify(jsonText));
    return jsonText;
});



function getTimeDay() {
    let timeNow = (Timer.now() + stateSwitchEsp.timeZone * 60 * 60) % 86400;
    return timeNow;
}

function controlLight() {
    let timeSetParse = JSON.parse(Cfg.get('input1'));
    let timeNow = getTimeDay();
    let timeFromSet = JSON.parse(timeSetParse.from.hours) * 60 * 60 + JSON.parse(timeSetParse.from.minut) * 60 + JSON.parse(timeSetParse.from.second);
    let timeToSet = JSON.parse(timeSetParse.to.hours) * 60 * 60 + JSON.parse(timeSetParse.to.minut) * 60 + JSON.parse(timeSetParse.to.second);
    if (timeFromSet < timeToSet) {
        if (timeNow > timeFromSet && timeNow < timeToSet) {
            if (switchPin(stateSwitchEsp.pins[1], true, "/light", stateSwitchEsp.input1) === true) {  //
                stateSwitchEsp.input1 = true;
            }
        } else {
            if (switchPin(stateSwitchEsp.pins[1], false, "/light", stateSwitchEsp.input1) === true) {  //
                stateSwitchEsp.input1 = false;
            }
        }
    } else if (timeFromSet > timeToSet) {
        if (timeNow < timeFromSet && timeNow > timeToSet) {
            if (switchPin(stateSwitchEsp.pins[1], false, "/light", stateSwitchEsp.input1) === true) {  //
                stateSwitchEsp.input1 = false;
            }
        } else {
            if (switchPin(stateSwitchEsp.pins[1], true, "/light", stateSwitchEsp.input1) === true) {  //
                stateSwitchEsp.input1 = true;
            }
        }
    } else {
        MQTT.pub("/light", "light switch off", 0);
    };
};

function switchPin(pin, statePin, topic, stateSwitch) {
    print ()
    if (stateSwitch !== statePin) {
        let statePinNamber = +statePin;
        GPIO.write(pin, statePinNamber);
        MQTT.pub(topic, JSON.stringify(statePin), 0);
        return true;
    } else {
        return false;
    };
};

function controlWatering() {
    let statusWaterign = checkStatusWatering();
    if (statusWaterign === true) {
        if (switchPin(stateSwitchEsp.pins[2], true, "/water", stateSwitchEsp.input1) === true) {  //
            stateSwitchEsp.input2 = true;
        }
    } else {
        if (switchPin(stateSwitchEsp.pins[2], false, "/water", stateSwitchEsp.input1) === true) {  //
            stateSwitchEsp.input2 = false;
        }
    };
};

function checkStatusWatering() {
    let setInput2 = JSON.parse(Cfg.get("input2"));
    let segmentWatering = 86400 / JSON.parse(setInput2.quantity);
    let currentTimeInSegment = getTimeDay() % segmentWatering;
    if (currentTimeInSegment < setInput2.duration) {
        return true;
    } else {
        return false;
    }
}




function getTime() {
    let timeNow = Timer.now() + 7 * 60 * 60;
    return timeNow;
}

function publishBme() {
    let bmeTemp = JSON.stringify(bme.readTemperature());
    let bmeHum = JSON.stringify(bme.readHumidity());
    let bmePres = JSON.stringify(bme.readPressure());
    MQTT.pub('/temperature', bmeTemp, 0);
    MQTT.pub('/humidity', bmeHum, 0);
    MQTT.pub('/pressure', bmePres, 0);
}

Timer.set(1 * 1000, true, function () {
    controlLight();
    controlWatering();
}, null);

//Timer.set(60 * 1000, true, function () {
 //   publishBme();
//}, null);



