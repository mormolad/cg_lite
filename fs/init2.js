load('api_mqtt.js');
load('api_timer.js');
load('api_sys.js');
load('api_config.js');
load('api_rpc.js');
load('api_gpio.js');
load('api_arduino_bme280.js');

let regulatorChannel = {
    input1: {
        nameChannel: 'light',
        pin: 12,
        currentStateRegulator: 0,
        stateRelay: 0,
        initChannel: GPIO.setup_output(regulatorChannel.input1.pin, 1),
        stateChannel: 'off',
    },
    input2: {
        nameChannel: 'watering',
        pin: 14,
        currentStateRegulator: 0,
        stateRelay: 0,
        initChannel: GPIO.setup_output(regulatorChannel.input2.pin, 1),
        stateChannel: 'off',
    },
    input3: {
        nameChannel: 'heating',
        pin: 13,
        currentStateRegulator: 0,
        stateRelay: 0,
        initChannel: GPIO.setup_output(regulatorChannel.input3.pin, 1),
        stateChannel: 'off',
    },
    input4: {
        nameChannel: 'humidification',
        pin: 15,
        currentStateRegulator: 0,
        stateRelay: 0,
        initChannel: GPIO.setup_output(regulatorChannel.input4.pin, 1),
        stateChannel: 'off',
    },
};



let bme = Adafruit_BME280.createI2C(0x76);
let bmeValue = null;

RPC.addHandler('myRpc', function (jsonText) {
    let textSaved = JSON.stringify(jsonText);
    if (jsonText.id === 'input1') {
        Cfg.set({ input1: textSaved });
    } else if (jsonText.id === 'input2') {
        Cfg.set({ input2: textSaved });
    } else if (jsonText.id === 'input3') {
        Cfg.set({ input3: textSaved });
    } else if (jsonText.id === 'input4') {
        Cfg.set({ input4: textSaved });
    };
    print(JSON.stringify(jsonText));
    return jsonText;
});




function controlLight() {
    regulatorChannel.input1.stateRelay = GPIO.read(regulatorChannel.input1.pin);
    let topic = '/' + regulatorChannel.input1.nameChannel;
    let timeSetParse = JSON.parse(Cfg.get('input1'));
    let timeFromSet = JSON.parse(timeSetParse.from.hours) * 60 * 60 + JSON.parse(timeSetParse.from.minut) * 60 + JSON.parse(timeSetParse.from.second);
    let timeToSet = JSON.parse(timeSetParse.to.hours) * 60 * 60 + JSON.parse(timeSetParse.to.minut) * 60 + JSON.parse(timeSetParse.to.second);
    
    function regulate() {
        if (timeNow > timeFromSet && timeNow < timeToSet) {
            updateState(true);
        } else if (timeNow < timeFromSet || timeNow > timeToSet) {
            updateState(false);
        };
    }
    
    function updateState(newState) {
    
        if (regulatorChannel.stateRelay !== newState) {
            GPIO.write(14, newState);
            MQTT.pub(topic, newState.toString(), 0);
            regulatorChannel.stateRelay = newState
        }
    }
    
    
    
    
    
    
    if (timerFromSet === timeToSet && regulatorChannel.input1.stateChannel !== 'off') {
        MQTT.pub('/alert', 'input1 off', 0);
        regulatorChannel.input1.stateChannel = 'off';
    } else if (timeFromSet < timeToSet) {
        if (timeNow > timeFromSet && timeNow < timeToSet && regulatorChannel.input1.stateRelay !== regulatorChannel.input1.currentStateRegulator) {
            regulatorChannel.input1.currentStateRegulator = 1;
            GPIO.write(14, 0);
            MQTT.pub(topic, 'true', 0);
        } else if ((timeNow < timeFromSet || timeNow > timeToSet) && regulatorChannel.input1.stateRelay !== regulatorChannel.input1.currentStateRegulator) {
            regulatorChannel.input1.currentStateRegulator = 0;
            GPIO.write(14, 1);
            MQTT.pub(topic, 'false', 0);
        };
    } else {
        if (timeNow > timeToSet && timeNow < timeFromSet && regulatorChannel.input1.stateRelay !== regulatorChannel.input1.currentStateRegulator) {
            regulatorChannel.input1.currentStateRegulator = 0;
            GPIO.write(14, 1);
            MQTT.pub(topic, 'false', 0)
        } else if ((timeNow < timeToSet || timeNow > timeFromSet) && regulatorChannel.input1.stateRelay !== regulatorChannel.input1.currentStateRegulator) {
            GPIO.write(14, 0);
            MQTT.pub(topic, 'true', 0)
        };
    };
};

function controlWatering() {
    let statusWaterign = checkStatusWatering();
    if (statusWaterign === true) {
        GPIO.write(pins.watering, 0);
        MQTT.pub("/water", true, 0);
    } else if (statusWaterign === false) {
        GPIO.write(pins.watering, 1);
        MQTT.pub("/water", false, 0);
    }

};

function checkStatusWatering() {
    let setInput2 = Cfg.get("input2");
    let segments = 86400 / setInput2.quantity;  // отрезки в секундах
    let segmentsWatering = setInput2.duration;
    let endSegmentOnWatering = [];
    let startSegmentOnWatering = [];
    let check = null;


    for (let i = 0; i > setInput2.quantity; i++) {
        startSegmentOnWatering[i] = segments * i;
        endSegmentOnWatering[i] = segments * i + setInput2.duration;

        if (getTimeDay() > startSegmentOnWatering && getTime() < endSegmentOnWatering) {
            chek++1;
        };

    };
    if (check > 0) {
        return true;
    } else {
        return false;
    };
}


function getTimeDay() {
    let timeNow = (Timer.now() + 7 * 60 * 60) % 86400;
    return timeNow;
}

function getTime() {
    let timeNow = Timer.now() + 7 * 60 * 60;
    return timeNow;
}

Timer.set(5 * 1000, true, function () {
    let timeNowEsp = JSON.stringify(getTime());
    let pressure = bme.readPressure();
    let bmeTemp = JSON.stringify(bme.readTemperature());
    let bmeHum = JSON.stringify(bme.readHumidity());
    MQTT.pub('/temperature', bmeTemp, 0);
    MQTT.pub('/humidity', bmeHum, 0);
    MQTT.pub('/dateEsp', timeNowEsp, 0);
    controlLight();
    controlWatering();
}, null);
